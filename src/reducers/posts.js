const _ = require('lodash');
const api = require('../helpers/api');

// Action type constants
const INSERT = 'blog-frontend/posts/INSERT';
const CHANGE = 'blog-frontend/posts/CHANGE';
const REMOVE = 'blog-frontend/posts/REMOVE';

// The initial state of blog post data
const initialState = {
  visiblePosts: [],
  postData: {}
};

// Function which takes the current data state and an action,
// and returns a new state
function reducer(state, action) {
  state = state || initialState;
  action = action || {};

  switch(action.type) {
    // Inserts new posts into the local store
    case INSERT: {
      // Update post data
      const postData = _.assign({}, state.postData, _.keyBy(action.posts, 'id'));
      // Get list of IDs from posts to insert
      const newPostIds = _.map(action.posts, post => post.id);
      // Add new post IDs to list of visible posts, and sort
      // by creation date
      const visiblePosts = _.orderBy(
        _.uniq(state.visiblePosts.concat(newPostIds)),
        (postId) => postData[postId].createdAt,
        'desc');
      // Return updated state
      return _.assign({}, state, { visiblePosts, postData });
    }
    // Changes a single post's data in the local store
    case CHANGE: {
      const postData = _.clone(state.postData);
      postData[action.post.id] = action.post;
      return _.assign({}, state, { postData });
    }
    // Removes a single post from the visible post list
    // NOTE: Currently post data is still retained in store
    case REMOVE: {
      const visiblePosts = _.without(state.visiblePosts, action.id);
      return _.assign({}, state, { visiblePosts });
    }

    default: return state;
  }
}

// Now we define a whole bunch of action creators

// Inserts posts into the post list
reducer.insertPosts = (posts) => {
  return { type: INSERT, posts };
};

// Removes a post from the visible post list
reducer.removePost = (id) => {
  return { type: REMOVE, id };
};

// Attempts to delete a post from the server and removes it from the visible
// post list if successful
reducer.deletePost = (postId) => {
  return (dispatch) => {
    api.delete('/posts/' + postId).then(() => {
      dispatch(reducer.removePost(postId));
    }).catch(() => {
      alert('Failed to delete post');
    });
  };
};

// Attempts to update a post on the server and updates local post data if
// successful
reducer.savePost = (editedPost, callback) => {
  return (dispatch) => {
    api.put('/posts/' + editedPost.id, editedPost).then((post) => {
      dispatch(reducer.changePost(post));
      callback();
    }).catch(() => {
      alert('Failed to save post.  Are all of the fields filled in correctly?');
    });
  };
};

// Attempts to create a post on the server and inserts it into the local post
// list if successful
reducer.createPost = (newPost, callback) => {
  return (dispatch) => {
    api.post('/posts', newPost).then((post) => {
      dispatch(reducer.insertPosts([post]));
      callback();
    }).catch(() => {
      alert('Failed to create post. Are all of the fields filled in correctly?');
    });
  };
};

// Changes local post data
reducer.changePost = (post) => {
  return { type: CHANGE, post };
};

// Attempts to load more posts from the server and inserts them into the local
// post list if successful
reducer.loadMorePosts = (callback) => {
  return (dispatch, getState) => {
    const state = _.assign({}, initialState, getState().posts);

    const oldestPost = state.postData[_.last(state.visiblePosts)];
    const path = '/posts?olderThan=' + oldestPost.createdAt;

    api.get(path).then((newPosts) => {
      dispatch(reducer.insertPosts(newPosts));
      callback();
    }).catch(() => {
      alert('Failed to load more posts');
      callback('Failed to load more posts');
    });
  };
};

// Export the action creators and reducer
module.exports = reducer;
