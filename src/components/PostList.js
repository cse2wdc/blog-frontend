const React = require('react');
const ReactCSSTransitionGroup = require('react-addons-css-transition-group');
const ReactRedux = require('react-redux');
const _ = require('lodash');

const postsActionCreators = require('../reducers/posts');
const createActionDispatchers = require('../helpers/createActionDispatchers');
const Post = require('./Post');
const PostNew = require('./PostNew');

/**
 * A list of blog posts, along with buttons for writing a new post
 * and loading more posts.
 */
const PostList = React.createClass({
  displayName: 'PostList',
  // Set initial internal state for this component
  getInitialState: function() {
    return { loading: false };
  },
  onLoadButtonClick: function() {
    // If we are not already in the process of loading posts,
    // start loading more posts.
    if(!this.state.loading) {
      this.setState({ loading: true });
      this.props.loadMorePosts(() => {
        this.setState({ loading: false });
      });
    }
  },
  // Function which creates a post component from a post ID
  createPostComponent: function(postId) {
    return (
      <Post
        key={postId}
        post={this.props.posts.postData[postId]}
        time={this.props.time}
        savePost={this.props.savePost}
        deletePost={_.partial(this.props.deletePost, postId)}
      />
    );
  },
  render: function() {
    return (
      <div className="row">
        <div className="blog-main">
          {/* Button for writing a new post */}
          <PostNew
            createPost={this.props.createPost}
          />
          {/* Actual list of posts (with snazzy animations!) */}
          <ReactCSSTransitionGroup transitionName="blog-post-anim"
            transitionEnterTimeout={500} transitionLeaveTimeout={300}
          >
            { this.props.posts.visiblePosts.map(this.createPostComponent) }
          </ReactCSSTransitionGroup>
          {/* Button for loading more posts */}
          <button className="blog-load-more btn btn-default btn-lg"
            onClick={this.onLoadButtonClick}
            disabled={this.state.loading}
          >
            {this.state.loading ? 'Loading...' : 'Load more posts'}
          </button>
        </div>
      </div>
    );
  }
});

// Connect PostList component to the Redux store
const PostListContainer = ReactRedux.connect(
  // Map store state to props
  (state) => ({
    posts: state.posts,
    time: state.time
  }),
  createActionDispatchers(postsActionCreators)
)(PostList);

module.exports = PostListContainer;
