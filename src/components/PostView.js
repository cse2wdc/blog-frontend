const React = require('react');
const moment = require('moment');

/**
 * Render edit/delete buttons and post timestamp.
 */
const PostMeta = ({ post, time, onEdit, onDelete }) => (
  <div className="blog-post-meta">
    <a role="button" title="Edit post"
      style={{ paddingRight: '8px' }}
      onClick={ onEdit }
    >
      <span className="fa fa-edit" />
    </a>
    <a role="button" title="Delete post"
      style={{ paddingRight: '8px' }}
      onClick={ onDelete }
    >
      <span className="fa fa-remove" />
    </a>
    { moment(post.createdAt).from(time.now) }
  </div>
);

/**
 * A read-only view of a blog post.
 */
const PostView = ({ post, time, onEdit, onDelete }) => (
  <div className="blog-post">
    {/* TODO: Display blog title */}
    {/* TODO: Display blog metadata */}
    {/* TODO: Display blog content */}
  </div>
);

module.exports = PostView;
