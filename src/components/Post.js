const React = require('react');

const PostEdit = require('./PostEdit');
const PostView = require('./PostView');

const Post = React.createClass({
  displayName: 'Post',
  getInitialState: function() {
    return { editing: false };
  },
  openEdit: function() {
    this.setState({ editing: true });
  },
  closeEdit: function() {
    this.setState({ editing: false });
  },
  saveEdit: function(editedPost) {
    this.props.savePost(editedPost, (err) => {
      if(!err) this.closeEdit();
    });
  },
  render: function() {
    if(this.state.editing) {
      // Render component for editing the post
      return (
        <PostEdit
          post={this.props.post}
          onSave={this.saveEdit}
          onCancel={this.closeEdit}
        />
      );
    }
    // Render read-only view of the post
    return (
      <PostView
        post={this.props.post}
        time={this.props.time}
        onDelete={this.props.deletePost}
        onEdit={this.openEdit}
      />
    );
  }
});

// Export the Post component
module.exports = Post;
